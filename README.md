# iot-device-2


This device has two modes, 'ADL' and 'random'. The 'ADL' mode will receive the ADL data from IoT_1 and will send the labels to the Timescale DB. This happens whenever a volunteer completely finishes one of his/her activities.

For the random data, it generates PH values based on the Gaussian distribution. It will choose to send them over to the Analytics device, based on the Temperature data received from IoT_1. If the temperature received is over 4.0 degrees it will be sent over to the Analytics. The device sleeps for 7 seconds and wakes up to check the temperature data received from IoT_1, using the 'mid_sample' topic. If the temperature is over 4.0, it will send the pH data to the 'sample2' topic using an array of pH values. Also, the device will send the same ph value in the 'ph_01' topic to be consumed from the Timescale DB (for more on that, check the database-recorder repo). When the device reaches the last value of the array, a new array with fresh values will be generated. Whenever the door of IoT_1 is closed, this IoT device won't be able to read any data. If you want to check when the door of IoT_1 closes, check the README.md file of IoT_1.

Note: Due to the handling of consumed Python-Kafka messages, the first message by IoT_1 will always arrive immediately and the 7-second delay will be ignored. Otherwise, the 7 second delay will cause this device to ignore the first received message. 

Details about the installation of Kubernetes and Kafka can be seen Kubernetes-Kafka-Installation.txt. For how to deploy a python script as k8s pod, see: Kubernetes-Deployment-with-GitLab-CI.txt

How to use the device: Originally, the deployment of the device was done by the CI/CD system of GitLab. Now that there are two modes, the deployment is done by the CLI (since we may need different arguments each time). If you want to deploy the device and use it on the ADL data you will need to type the arguments: 'adl' and the volunteer id (m1-11 and f1-5). For the random data, you'll need the 'random' argument. Here's an example use of it: `kubectl run iot2 --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-2:<latest-tag> --overrides='{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }' --command -- python3 /opt/vb/iot2.py adl f1`.

Keep in mind that whenever you update the device, you will need to add a new tag.

If you want to revert to the deployment through GitLab, update the Dockerfile and change the line: `ENTRYPOINT ["python3", "/opt/vb/iot1.py"]` to `CMD ["python3", "/opt/vb/iot1.py"]`, add the arguments you need, and uncomment the 'deploy' job of the .gitlab-ci.yml file.
