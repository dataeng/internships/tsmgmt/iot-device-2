FROM ubuntu:latest
RUN apt-get update && apt-get -y install python3 && apt-get -y install python3-pip
RUN pip3 install scipy
RUN pip3 install kafka-python
RUN pip3 install psycopg2-binary
COPY ./ /opt/vb
ENV PYTHONUNBUFFERED=1
CMD ["python3", "/opt/vb/iot2.py"]
