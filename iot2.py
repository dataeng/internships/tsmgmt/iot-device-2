# Import KafkaProducer and KafkaConsumer from Kafka library
from kafka import KafkaProducer
from kafka import KafkaConsumer
from json import loads
from json import dumps
from scipy.stats import truncnorm
from scipy.stats import gamma
import time, json, re, random, datetime, os, sys, math, psycopg2


# Define server with port
producer_server = ['kafka-0.kafka-headless.default.svc.cluster.local:9092']
consumer_server = ['kafka.default.svc.cluster.local:9092']
#producer_server = ['localhost:9092'] #(Switch to this for local runs)
#consumer_server = producer_server

# Define topic name where the device will receive data

# Initialize producer and consumer variables
producer = KafkaProducer(bootstrap_servers = producer_server,value_serializer=lambda x: 
                         dumps(x).encode('utf-8'))


if len(sys.argv) > 1:

  #Starting off with the ADL-related data.
  subject_arg = ''
  if sys.argv[1] == 'adl' or sys.argv[1] == 'ADL':
    subject_arg = sys.argv[2]

    #Argument checking.
    if subject_arg[0] != 'm' and subject_arg[0] != 'f':
      print("Invalid arguments. Choose between m or f.")
      sys.exit()
      
      if subject_arg[0] == 'm':
        if subject_arg[1:].isnumeric() != True:
          print("Invalid arguments. Male subjects are between 1 and 11")
          sys.exit()
        if int(subject_arg[1:]) < 0 or int(subject_arg[1:]) > 11:
          print("Invalid arguments. Male subjects are between 1 and 11")
          sys.exit()

      if subject_arg[0] == 'f':
        if subject_arg[1:].isnumeric() != True:
          print("Invalid arguments. Female subjects are between 1 and 5")
          sys.exit()
        if int(subject_arg[1:]) < 0 or int(subject_arg[1:]) > 5:
          print("Invalid arguments. Female subjects are between 1 and 5")
          sys.exit()

    #Deleting pre-existing label data to avoid duplicates.
    conn = psycopg2.connect(
    host="tsdb-1.default.svc.cluster.local",
    database="iotdb",
    user="postgres",
    password="psqlpass")

    query = 'SELECT count(*) > 0 FROM adl_labels_01 WHERE volunteer = %s'
    user = subject_arg
    cur = conn.cursor()
    cur.execute(query, (user,))
    query_results = cur.fetchall()
    if query_results[0][0] == True:
      print('This volunteer has already recorded their data. Deleting the existing records for a fresh deployment...')
      cur = conn.cursor()
      query = 'DELETE FROM adl_labels_01 WHERE volunteer = %s'
      cur.execute(query, (user,))
      conn.commit()


    topicName = 'adl_mid_topic' 
    consumer = KafkaConsumer(topicName, bootstrap_servers= consumer_server,
        value_deserializer=lambda x: loads(x.decode('utf-8')))

    #This array will keep all of the subject's final files for each activity he/she participated in.
    labels_arr = []

    #Here we iterate every file to find the subject's final file of each activity and add it in the array.
    for folder in os.listdir("/opt/vb/ADL_dataset"):
      subject_files = [] #This array will keep all of the subject's files for one activity in a sorted temporal order.
      nested_dir = "/opt/vb/ADL_dataset/" + folder  
      for filename in os.listdir(nested_dir):
        
        name_split = filename.split('-')
        subject = name_split[8].replace('.txt','')
        
        if subject == subject_arg:
          date = ''
          for date_elem in name_split[1:7]:
            date += date_elem + '-'
          date = date[:-1]

          date_obj = datetime.datetime.strptime(date, "%Y-%m-%d-%H-%M-%S")
          filename = nested_dir + '/' + filename
          subject_files.append((filename, date_obj))
          subject_files = sorted(subject_files, key=lambda x: x[1])
      
      #If there is at least one file for this activity we will open it and get the timestamp of the last acceloremeter data-line. (This is the timestamp when the subject completely finishes one of his activities) 
      if subject_files != []:
        
        lastfile = subject_files[-1]
        lines = sum(1 for line in open(lastfile[0]))
        hz_final = (lines-1) * 0.03125 #Getting the last data-line's seconds by multiplying its number with 32 hz (0.03125 sec)
        
        date = lastfile[1].strftime("%Y-%m-%dT%H:%M:%S")
        date = date + '.0'
        datetime_object = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%f')
        datetime_object = datetime_object + datetime.timedelta(seconds=hz_final) #Adding the last timestamp's seconds to the timestamp of the file's title.
        labels_arr.append((lastfile[0],datetime_object))

    #Now we read the data of iot-1
    for msg in consumer:

      json_data = msg.value
      filename = json_data['filename']
      date = json_data['date']
      x = json_data['x']
      y = json_data['y']
      z = json_data['z']

      datetime_object = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%S.%f')
      file_date_item = (filename,datetime_object)
      
      #If the given filename+date is in the array with the labels we have one of the finishing timestamps for this subject's activity. So we get the data we need and we send them to the DB.  
      if file_date_item in labels_arr:
        
        name_split = filename.split('-')
        subject = name_split[8].replace('.txt','')
        activity = name_split[7]

        db_set = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "x", "type": "int32", "optional": False },{"field": "y", "type": "int32", "optional": False },{"field": "z", "type": "int32", "optional": False },{"field": "activity", "type": "string", "optional": False },{"field": "volunteer", "type": "string", "optional": False },{"field": "time", "type": "string", "optional": False }]},"payload": {"x":x,"y":y,"z":z, "activity":activity,"volunteer":subject,"time":date}} 
        producer.send('adl_labels_01', db_set)
      
    #Keeping the container running for k8s.
    while True:
      time.sleep(5)
      print('Keeping the container running.')

  #Now moving on to random data.
  elif sys.argv[1] == 'random':
    
    topicName = 'mid_sample' 
    consumer = KafkaConsumer(topicName, bootstrap_servers= consumer_server,
        value_deserializer=lambda x: loads(x.decode('utf-8')))
      

    def get_truncated_normal(mean=0, sd=1, low=0, upp=10): #Truncated normal distribution is used in order to get the values in the range we need.
        return truncnorm(
            (low - mean) / sd, (upp - mean) / sd, loc=mean, scale=sd)

    #Getting 1000 values based on the Gaussian distribution.
    def get_arr(): 
      X = get_truncated_normal(mean=6.4, sd=0.15, low=0, upp=14)
      ph_arr = X.rvs(1000)
      ph_arr = [ '%.2f' % elem for elem in ph_arr ] #Double decimal.
      return ph_arr

    #Defining the array
    ph_arr = get_arr()

    ph_counter = 0 #Used to get the next values of the ph array
    #start = time.time()

    i = 0
    while True: #Two loops are needed. The while-loop makes the device sleep for 7 seconds and the for-loop picks up every message.
      print('rand')
      i += 1 
      if i != 1: #The first message from IoT1 must arrive immediatly, otherwise this device will sleep for 7 sec and ignore the first message that was written in the buffer.
        time.sleep(7)
      
      for msg in consumer:

        json_data = msg.value
        temp = float(json_data['temperature'])
      
        #Sending the PH value we have to Analytics device, only if the temperature is over 4.0
        if temp > 4.0:
          ph_item = ph_arr[ph_counter] #Getting an element from the ph array.
          ph_counter += 1
          
          if ph_counter == 999: #Updating the array at every 1000 elements.
            ph_arr = get_arr()
            ph_counter = 0
          
          ph_set = {"topic":'ph','ph': ph_item} #Send the data as a json object
          ph_set_db = {"schema": {"type": "struct", "optional": False, "fields": [{"field": "ph", "type": "float", "optional": False }]},"payload": {"ph": float(ph_item)}}
          print('The temperature from IoT_1 is over 4.0 (',temp,'). Sending ph ',ph_item,' over to Analytics... ')
          producer.send('ph_01', ph_set_db)
          producer.send('sample2', ph_set)
        else:
          print('The temperature from IoT_1 is not over 4.0 (',temp,'). Waiting for new values... ')

        #end = time.time()
        #print(end-start)

        #We have to break the loop in order for the device to sleep again.
        break
  else:
    print("Invalid arguments. Choose between adl/ADL or random")
    sys.exit()


